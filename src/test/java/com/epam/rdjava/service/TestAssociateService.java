package com.epam.rdjava.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.rdjava.dto.AssociateDto;
import com.epam.rdjava.dto.BatchDto;
import com.epam.rdjava.dto.ExceptionResponse;
import com.epam.rdjava.entity.Associate;
import com.epam.rdjava.entity.Batch;
import com.epam.rdjava.exception.AssociateException;
import com.epam.rdjava.exception.AssociateNotFoundException;
import com.epam.rdjava.repository.AssociateRepository;
import com.epam.rdjava.repository.BatchRepository;

@ExtendWith(MockitoExtension.class)
class TestAssociateService {

	@Mock
	private AssociateRepository associateRepository;

	@Mock
	private BatchRepository batchRepository;

	@Mock
	private ModelMapper modelMapper;

	@InjectMocks
	private AssociateServiceImpl associateService;

	private AssociateDto associateDto;

	private BatchDto batchDto;

	private Associate associate;

	private Batch batch;

	private List<AssociateDto> associatesDtoList = new ArrayList<>();

	private List<Associate> associatesList = new ArrayList<>();

	@BeforeEach
	void setData() throws Exception {
		associate = new Associate();
		associate.setAssociateId(1);
		associate.setCollege("college");
		associate.setEmail("sample@email.com");
		associate.setGender("male");
		associate.setStatus("status");
		associate.setName("name");
		batch = new Batch();
		batch.setBatchId(1);
		batch.setName("batch");
		batch.setPractice("JAVA");
		associate.setBatch(batch);
		associatesList.add(associate);
	}

	@BeforeEach
	void setDtoData() throws Exception {
		associateDto = new AssociateDto();
		associateDto.setAssociateId(1);
		associateDto.setCollege("college");
		associateDto.setEmail("sample@email.com");
		associateDto.setGender("male");
		associateDto.setStatus("status");
		associateDto.setName("name");
		batchDto = new BatchDto();
		batchDto.setBatchId(1);
		batchDto.setName("batch");
		batchDto.setPractice("JAVA");
		associateDto.setBatch(batchDto);
		associatesDtoList.add(associateDto);
	}

	@Test
	void testAddAssociate() {
		associate.setAssociateId(null);
		when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDto, associateService.addAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(modelMapper).map(associate,AssociateDto.class);
		verify(associateRepository).save(associate);
	}

	@Test
	void testAddAssociate_BatchExists()
	{
		when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
		when(modelMapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		when(batchRepository.findById(associateDto.getBatch().getBatchId())).thenReturn(Optional.ofNullable(batch));
		when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDto, associateService.addAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(batchRepository).findById(any(Integer.class));
		verify(modelMapper).map(associate,AssociateDto.class);
	}

	@Test
	void testAddAssociate_Invalid()
	{
		when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
		when(batchRepository.findById(associateDto.getBatch().getBatchId())).thenReturn(Optional.ofNullable(batch));
		when(associateRepository.save(associate)).thenThrow(DataIntegrityViolationException.class);
		assertThrows(AssociateException.class,()-> associateService.addAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(batchRepository).findById(any(Integer.class));
		verify(associateRepository).save(associate);
		verify(modelMapper,never()).map(associate,AssociateDto.class);
	}

	@Test
	void testGetAssociatesByGender()
	{
		when(modelMapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		when(associateRepository.findByGender(any(String.class))).thenReturn(associatesList);
		assertEquals(associatesDtoList, associateService.getAssociates("male"));
		verify(associateRepository).findByGender(any(String.class));
		verify(modelMapper).map(associate,AssociateDto.class);
	}

	@Test
	void testDeleteAssociate() {
		doNothing().when(associateRepository).deleteById(any(Integer.class));
		associateService.deleteAssociate(2);
		verify(associateRepository).deleteById(2);
	}

	@Test
	void testUpdateAssociate()
	{
		when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
		when(modelMapper.map(associate,AssociateDto.class)).thenReturn(associateDto);
		when(associateRepository.existsById(associate.getAssociateId())).thenReturn(true);
		when(associateRepository.save(associate)).thenReturn(associate);
		assertEquals(associateDto, associateService.updateAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(modelMapper).map(associate,AssociateDto.class);
		verify(associateRepository).existsById(associate.getAssociateId());
		verify(associateRepository).save(associate);
	}

	@Test
	void testUpdateAssociate_AssociateNotExists()
	{
		when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
		when(associateRepository.existsById(associate.getAssociateId())).thenReturn(false);
		assertThrows(AssociateNotFoundException.class, ()->associateService.updateAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(modelMapper,never()).map(associate,AssociateDto.class);
		verify(associateRepository).existsById(associate.getAssociateId());
		verify(associateRepository,never()).save(associate);
	}

	@Test
	void testUpdateAssociate_Invalid()
	{
		when(modelMapper.map(associateDto,Associate.class)).thenReturn(associate);
		when(associateRepository.existsById(associate.getAssociateId())).thenReturn(true);
		when(associateRepository.save(associate)).thenThrow(DataIntegrityViolationException.class);
		assertThrows(AssociateException.class,()-> associateService.updateAssociate(associateDto));
		verify(modelMapper).map(associateDto,Associate.class);
		verify(modelMapper,never()).map(associate,AssociateDto.class);
		verify(associateRepository).existsById(associate.getAssociateId());
		verify(associateRepository).save(associate);
	}
	
	@BeforeAll
	static void setSampleData()
	{
		Batch batch=new Batch();
		BatchDto batchDto=new BatchDto();
		batchDto.setStartDate(new Date());
		batchDto.setEndDate(new Date());
		batch.setStartDate(new Date());
		batch.setEndDate(new Date());
		ExceptionResponse response=new ExceptionResponse("","","","");
		response.setError("error");
		response.setStatus("status");
		response.setPath("path");
		response.setTimestamp("timestamp");
		batch.getAssociates();
		batch.setAssociates(null);
		batch.getBatchId();
		batch.getEndDate();
		batch.getName();
		batch.getPractice();
		batch.getStartDate();
	}

}
