package com.epam.rdjava.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.rdjava.dto.AssociateDto;
import com.epam.rdjava.dto.BatchDto;
import com.epam.rdjava.exception.AssociateException;
import com.epam.rdjava.exception.AssociateNotFoundException;
import com.epam.rdjava.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class TestAssociateController {

	@MockBean
	private AssociateService associateService;

	@Autowired
	private MockMvc mockMvc;

	private AssociateDto associateDto;

	private BatchDto batchDto;

	private ObjectMapper mapper;

	private String associateJsonString;

	private List<AssociateDto> associatesList = new ArrayList<>();

	@BeforeEach
	void setData() throws Exception {
		associateDto = new AssociateDto();
		associateDto.setAssociateId(1);
		associateDto.setCollege("college");
		associateDto.setEmail("sample@email.com");
		associateDto.setGender("male");
		associateDto.setStatus("status");
		associateDto.setName("name");
		batchDto = new BatchDto();
		batchDto.setBatchId(1);
		batchDto.setName("batch");
		batchDto.setPractice("JAVA");
		associateDto.setBatch(batchDto);
		mapper = new ObjectMapper();
		associateJsonString = mapper.writeValueAsString(associateDto);
		associatesList.add(associateDto);
	}

	@Test
	void testAddAssociate() throws Exception 
	{
		when(associateService.addAssociate(any(AssociateDto.class))).thenReturn(associateDto);
		MvcResult result =mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString)).andExpect(status().isCreated()).andReturn();
		assertEquals(result.getResponse().getContentAsString(),associateJsonString);
		verify(associateService).addAssociate(any(AssociateDto.class));
	}

	@Test
	void testAddAssociate_Invalid() throws Exception {
		associateDto.setCollege(null);
		associateJsonString = mapper.writeValueAsString(associateDto);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString))
				.andExpect(status().isBadRequest()).andReturn();
		verify(associateService, never()).addAssociate(any(AssociateDto.class));
	}

	@Test
	void testAddAssociate_Duplicate() throws Exception 
	{
		when(associateService.addAssociate(any(AssociateDto.class))).thenThrow(AssociateException.class);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString)).andExpect(status().isBadRequest());
		verify(associateService).addAssociate(any(AssociateDto.class));
	}

	@Test
	void testAddAssociate_UnknownException() throws Exception 
	{
		when(associateService.addAssociate(any(AssociateDto.class))).thenThrow(RuntimeException.class);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString)).andExpect(status().isInternalServerError());
		verify(associateService).addAssociate(any(AssociateDto.class));
	}

	@Test
	void testGetAssociateByGender() throws Exception
	{
		when(associateService.getAssociates(any(String.class))).thenReturn(associatesList);
	 	MvcResult result = mockMvc.perform(get("/rd/associates/male")).andExpect(status().isOk()).andReturn();
	 	assertEquals(mapper.writeValueAsString(associatesList), result.getResponse().getContentAsString());
	 	verify(associateService).getAssociates(any(String.class));
	}

	@Test
	void testUpdateAssociate() throws Exception 
	{
		when(associateService.updateAssociate(any(AssociateDto.class))).thenReturn(associateDto);
		MvcResult result =mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString)).andExpect(status().isOk()).andReturn();
		assertEquals(result.getResponse().getContentAsString(),associateJsonString);
		verify(associateService).updateAssociate(any(AssociateDto.class));
	}

	@Test
	void testUpdateAssociate_Invalid() throws Exception 
	{
		when(associateService.updateAssociate(any(AssociateDto.class))).thenThrow(AssociateNotFoundException.class);
		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(associateJsonString)).andExpect(status().isNotFound()).andReturn();
		verify(associateService).updateAssociate(any(AssociateDto.class));
	}

	@Test
	void testDeleteAssociate() throws Exception {
		doNothing().when(associateService).deleteAssociate(1);
		mockMvc.perform(delete("/rd/associates/1")).andExpect(status().isNoContent());
	}
 
}
