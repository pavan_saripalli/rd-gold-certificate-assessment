package com.epam.rdjava.entity;

import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity 
@Table
@Getter
@Setter
@NoArgsConstructor
public class Batch {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer batchId;
	
	@Column(unique=true)
	private String name;
	
	private String practice;
	
	private Date startDate;
	
	private Date endDate;
	
	@OneToMany(mappedBy = "batch")
	private List<Associate> associates;

	@Override
	public String toString() {
		return "Batch [batchId=" + batchId + ", name=" + name + ", practice=" + practice + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
	

}
