package com.epam.rdjava.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.epam.rdjava.dto.AssociateDto;
import com.epam.rdjava.entity.Associate;
import com.epam.rdjava.exception.AssociateException;
import com.epam.rdjava.exception.AssociateNotFoundException;
import com.epam.rdjava.repository.AssociateRepository;
import com.epam.rdjava.repository.BatchRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class AssociateServiceImpl implements AssociateService {

	private final AssociateRepository associateRepository;
	private final BatchRepository batchRepository;
	private final ModelMapper modelMapper;

	@Override
	public AssociateDto addAssociate(AssociateDto associateDto) {
		Associate associate = modelMapper.map(associateDto, Associate.class);
		log.info("adding associate {}", associate);
		if (associate.getBatch().getBatchId() != null) {
			batchRepository.findById(associateDto.getBatch().getBatchId()).ifPresent(associate::setBatch);
		}
		try {
			Associate addedAssociate = associateRepository.save(associate);
			log.info("associate added successfully {}", associate);
			return modelMapper.map(addedAssociate, AssociateDto.class);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage());
			throw new AssociateException(
					"Associate name, email should be unique, and Same batch names are not allowed for multiple batches");
		}
	}

	@Override
	public List<AssociateDto> getAssociates(String gender) {
		log.info("fetching associate data by gender {}", gender);
		return associateRepository.findByGender(gender).stream()
				.map(associate -> modelMapper.map(associate, AssociateDto.class)).toList();
	}

	@Override
	public AssociateDto updateAssociate(AssociateDto associateDto) {
		log.info("Updating AssociateDto {}", associateDto);
		Associate associate = modelMapper.map(associateDto, Associate.class);
		if (!associateRepository.existsById(associate.getAssociateId())) {
			log.error("No Associate found with given Id");
			throw new AssociateNotFoundException("No Associate found with given Id");
		}
		try {
			Associate updatedAssociate = associateRepository.save(associate);
			log.info("associate data updated {}", updatedAssociate);
			return modelMapper.map(updatedAssociate, AssociateDto.class);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage());
			throw new AssociateException("Associate name, email should be unique");
		}
	}

	@Override
	public void deleteAssociate(Integer associateId) {
		log.info("deleting associate with id {}", associateId);
		associateRepository.deleteById(associateId);
	}

}
 