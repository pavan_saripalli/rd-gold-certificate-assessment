package com.epam.rdjava.service;

import java.util.List;

import com.epam.rdjava.dto.AssociateDto;

public interface AssociateService {

	AssociateDto addAssociate(AssociateDto associateDto);
	List<AssociateDto> getAssociates(String gender);
	AssociateDto updateAssociate(AssociateDto associateDto);
	void deleteAssociate(Integer associateId);
}
