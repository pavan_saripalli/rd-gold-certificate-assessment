package com.epam.rdjava.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(value = Include.NON_EMPTY)
@Getter
@Setter
@ToString
@NoArgsConstructor
public class BatchDto {
	
    private Integer batchId;
	
    @NotBlank
    @Size(min=2,max=100,message="Size of batch name should be 2- 100 characters")
	private String name;
	
    @NotBlank
	private String practice;
    
	private Date startDate;
	
	private Date endDate;

}
