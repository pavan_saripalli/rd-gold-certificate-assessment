package com.epam.rdjava.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(value = Include.NON_EMPTY)
@Getter
@Setter
@ToString
@NoArgsConstructor
public class AssociateDto {

	private Integer associateId;

	@NotBlank
	@Size(min=2,max=100,message = "The size of name should be 2-100 characters")
	private String name;

	@NotBlank
	@Email
	@Size(min=5,max=100,message = "The size of email should be 5-100 characters")
	private String email;
	
	@NotBlank
	private String gender;

	@NotBlank
	@Size(min=5,max=100,message="The size of college name should be 5-100 characters")
	private String college;
	
	@NotBlank
	private String status;

	private BatchDto batch;

}
