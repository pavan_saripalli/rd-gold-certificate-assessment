package com.epam.rdjava.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class ExceptionResponse {
	
	private String timestamp;
	private String status;
	private String error;
	private String path;

} 
