package com.epam.rdjava.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rdjava.entity.Associate;

public interface AssociateRepository extends JpaRepository<Associate, Integer>{
	
	List<Associate> findByGender(String gender);

}
