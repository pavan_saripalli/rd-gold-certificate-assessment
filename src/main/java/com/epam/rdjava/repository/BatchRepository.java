package com.epam.rdjava.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rdjava.entity.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer> {

}
