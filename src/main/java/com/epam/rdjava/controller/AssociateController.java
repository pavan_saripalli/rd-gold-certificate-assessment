package com.epam.rdjava.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.rdjava.dto.AssociateDto;
import com.epam.rdjava.service.AssociateService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rd/associates")
@RequiredArgsConstructor
@Slf4j
public class AssociateController {

	private final AssociateService associateService;

	@PostMapping
	public ResponseEntity<AssociateDto> addAssociate(@RequestBody @Valid AssociateDto associateDto) {
		log.info("adding associate {}",associateDto);
		return new ResponseEntity<>(associateService.addAssociate(associateDto), HttpStatus.CREATED);
	}

	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getAssociatesByGender(@PathVariable String gender) {
		log.info("Getting the Associates by Gender {}",gender);
		return ResponseEntity.ok(associateService.getAssociates(gender));
	}

	@PutMapping
	public ResponseEntity<AssociateDto> updateAssociate(@RequestBody AssociateDto associateDto) {
		log.info("Updating associate {}",associateDto);
		return ResponseEntity.ok(associateService.updateAssociate(associateDto));
	}

	@DeleteMapping("/{associateId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteAssociate(Integer associateId) {
		log.info("Deleting associate by Id {}",associateId);
		associateService.deleteAssociate(associateId);
	}

}
