package com.epam.rdjava.exception;

public class AssociateException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AssociateException(String message)
	{
		super(message);
	}

}
