package com.epam.rdjava.exception;

public class AssociateNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AssociateNotFoundException(String message)
	{
		super(message);
	}

}
