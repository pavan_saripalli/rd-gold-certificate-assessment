package com.epam.rdjava.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.rdjava.dto.ExceptionResponse;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		ex.getAllErrors().forEach(error -> errors.add(error.getDefaultMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), errors.toString(),
				req.getDescription(false));
	}
	
	@ExceptionHandler({AssociateNotFoundException.class})
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionResponse handleNotFoundExceptions(RuntimeException ex,WebRequest req)
	{
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.toString(), ex.getMessage(),
				req.getDescription(false));
	}
	
	@ExceptionHandler({AssociateException.class,HttpMessageNotReadableException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleAssociateException(RuntimeException ex,WebRequest req)
	{
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), ex.getMessage(),
				req.getDescription(false));
	}
	
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleException(RuntimeException ex,WebRequest req)
	{
		return new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getMessage(),
				req.getDescription(false));
	}

}
